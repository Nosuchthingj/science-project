var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var haml = require('hamljs');
var fs = require('fs');

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {

      // rendering the haml
      var hamlView = fs.readFileSync('public/index.haml', 'utf8');
      res.end(haml.render(hamlView));
})
app.listen(80, function() {
    console.log("started")
})
